package main

import (
	"net/http"
	"log"
	"github.com/maxence-charriere/go-app/v9/pkg/app"
)

type Menu struct {
	app.Compo
}
func (h *Menu) Render() app.UI {
	return app.Div().
		Class("pure-menu pure-menu-horizontal").
		Body(
			app.A().Href("#").Class("pure-menu-heading pure-menu-link").Text("Click Me"),
			app.Ul().Class("pure-menu-list").Body(
				app.Li().Class("pure-menu-item").Body(
					app.A().Href("#").Class("pure-menu-link").Text("Click Me"),
				),
				app.Li().Class("pure-menu-item").Body(
					app.A().Href("#").Class("pure-menu-link").Text("Click Me 2"),
				),
				app.Li().Class("pure-menu-item").Body(
					app.A().Href("#").Class("pure-menu-link").Text("Click Me 3"),
				),
			),
		)
}

func main() {
	app.Route("/", &Menu{})
	app.RunWhenOnBrowser()

	// Server Implementation.
	http.Handle("/", &app.Handler{
		Name: "Hello",
		Description: "Hello",
		Styles: []string{
			"/web/css/pure-min.css",
		},
	})
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("%v", err)
	}
}